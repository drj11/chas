# chas — CHAracter Set

`chas` is a command line utility to select and output various character sets.

In this utility a character set is a simple sequence Unicode codepoints.

The output is a series of hex numbers (either 4 digit, or 6 if required),
one per line.

## Usage

    chas hft|monotype

One use of this utility is to check that all the characters
recommended by a retail point are present.
`chas monotype` will output the list of characters recommended by Monotype
(from https://foundrysupport.monotype.com/hc/en-us/articles/360029280752);
`ccmap font.otf` will output the list of characters in a font-file.
These can be combined in a logical set operation using `grep`:

    chas monotype | grep -v -F -x "$(ccmap font.otf)"

that command shows which characters are recommended by Monotype but not
present in the font-file `font.otf`.

## Unicode Collation Algorithm sorting

I haven't implemented this yet, but here's a note on how to do it in Python
(`plak` now implements this with `--order uca` but will fail if
`icu` is not installed).

    # brew install pkg-config icu4c
    # pip install --no-binary=:pyicu: pyicu
    import icu

    collator = icu.Collator.createInstance()
    # To sort a string:
    print("".join(sorted(cs, key=collator.getSortKey)))

# END
