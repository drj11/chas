package main

import (
	"flag"
	"fmt"
	"log"
)

func main() {
	ascii := ""
	for i := 32; i < 127; i++ {
		ascii += string(rune(i))
	}

	letters_lc := "abcdefghijklmnopqrstuvwxyz"
	letters_uc := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// letters in Latin-1 Supplement
	letters_latin1_lc := "µàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿßšž"
	// Weirdly no capital Y dieresis.
	letters_latin1_uc := "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŠŽ"

	letters_ascii := letters_lc + letters_uc
	letters_latin1 := letters_latin1_lc + letters_latin1_uc
	letters_8859_1 := letters_ascii + letters_latin1

	// I'm counting everything that's not a letter, not a superscript
	// things, and not a spacing version of a dacritic mark:

	punctuation_latin1 := "¡¢£¤¥¦§©«¬-®" + "°±¶·»¿" + "×÷"

	// plus circumflex and caron accent?
	hft_extra := "œŒſ‰•‹›‘’‚“”„…–—"

	hftRecommended := []rune(ascii + letters_8859_1 + punctuation_latin1 + hft_extra)

	flag.Parse()
	setName := flag.Arg(0)

	charset := []rune{}

	switch setName {
	case "ascii":
		charset = []rune(ascii)
	case "iso-8859-1":
		for i := rune(0x20); i < 0x100; i++ {
			if 0x7f <= i && i < 0xa0 {
				continue
			}
			charset = append(charset, i)
		}
	case "monotype":
		charset = MonotypeUnicodes
	case "hft":
		charset = hftRecommended
	default:
		log.Fatalf("Charset name «%s» is not recognised; sorry.\n", setName)
	}

	seen := map[rune]struct{}{}

	for _, r := range charset {
		if _, done := seen[r]; done {
			continue
		}
		fmt.Printf("%04X\n", r)
		seen[r] = struct{}{}
	}
}
